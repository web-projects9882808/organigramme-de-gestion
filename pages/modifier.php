<?php
require ('../inc/connexion.php');
$idPoste=$_GET["idPoste"];
function getPoste($idPoste){
    $requete="SELECT * FROM Poste WHERE IdPoste=".$idPoste;
    $sql=mysqli_query(dbconnect(),$requete);
    return $sql;
}
$poste = mysqli_fetch_assoc(getPoste($idPoste));
?>  
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modification de l'organiramme</title>
</head>
<body>
    <script type="text/javascript">
        var poste = JSON.parse(json_encode($poste));
        function getXhr() {
            var xhr; 
            try {  
                xhr = new ActiveXObject('Msxml2.XMLHTTP');   
            }
            catch (e) {
                try {   
                    xhr = new ActiveXObject('Microsoft.XMLHTTP'); 
                }
                catch (e2) {
                    try {  
                        xhr = new XMLHttpRequest();  
                    }
                    catch (e3) {  
                        xhr = false;   
                    }
                }
            }
            return xhr;
        }
        function deletePoste(idPoste) {
            var xhr = getXhr();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {

                    }
                }
            };
            xhr.open("GET", "../inc/effacerPoste.php?idPoste=" + idPoste, true);
            xhr.send(null);
        }
    </script>
    <p>Poste : <?php echo $poste["Nom"]; ?></p>
    <input type="button" value="Effacer le Poste" onclick="deletePoste(poste.IdPoste)">
</body>
</html>