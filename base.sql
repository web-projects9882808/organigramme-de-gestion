CREATE DATABASE organigramme;
USE organigramme;

CREATE TABLE Poste (
    IdPoste INT PRIMARY KEY auto_increment,
    Nom VARCHAR(50) NOT NULL
);

CREATE TABLE Personne (
    IdPersonne INT PRIMARY KEY auto_increment,
    Nom VARCHAR(50) NOT NULL,
    Prenom VARCHAR(50) NOT NULL,
    IdPoste INT NOT NULL,
    FOREIGN KEY (IdPoste) REFERENCES Poste(idPoste)
);

CREATE TABLE Organigramme (
    Niveau INT NOT NULL,
    IdPoste INT NOT NULL,
    IdPosteMere INT,
    FOREIGN KEY (IdPoste) REFERENCES Poste(idPoste)
);

CREATE OR REPLACE VIEW PosteOrganigramme_view AS 
    SELECT Poste.IdPoste, Nom, Niveau, IdPosteMere FROM 
    Poste JOIN Organigramme ON Poste.IdPoste=Organigramme.IdPoste;

INSERT INTO Poste VALUES (NULL, "AG");
INSERT INTO Poste VALUES (NULL, "CA");
INSERT INTO Poste VALUES (NULL, "DG");
INSERT INTO Poste VALUES (NULL, "DCAI");
INSERT INTO Poste VALUES (NULL, "DSI");
INSERT INTO Poste VALUES (NULL, "DAF");
INSERT INTO Poste VALUES (NULL, "DC");
INSERT INTO Poste VALUES (NULL, "DP");
INSERT INTO Poste VALUES (NULL, "DRH");
INSERT INTO Poste VALUES (NULL, "SSI");

INSERT INTO Organigramme VALUES (1, 1, 0);
INSERT INTO Organigramme VALUES (2, 2, 1);
INSERT INTO Organigramme VALUES (3, 3, 2);
INSERT INTO Organigramme VALUES (4, 4, 3);
INSERT INTO Organigramme VALUES (5, 5, 4);
INSERT INTO Organigramme VALUES (5, 6, 4);
INSERT INTO Organigramme VALUES (5, 7, 4);
INSERT INTO Organigramme VALUES (5, 8, 4);
INSERT INTO Organigramme VALUES (5, 9, 4);
INSERT INTO Organigramme VALUES (6, 10, 5);