<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/style.css">
    <title>Organigramme de Gestion</title>
</head>
<body>
    <script type="text/javascript">
        function getXhr() {
            var xhr; 
            try {  
                xhr = new ActiveXObject('Msxml2.XMLHTTP');   
            }
            catch (e) {
                try {   
                    xhr = new ActiveXObject('Microsoft.XMLHTTP'); 
                }
                catch (e2) {
                    try {  
                        xhr = new XMLHttpRequest();  
                    }
                    catch (e3) {  
                        xhr = false;   
                    }
                }
            } 
            return xhr;
        }
        window.addEventListener("load", function () {
            majTree();
        })
        function getLi(idPosteMere, callback) {
            var xhr = getXhr();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        var organigramme = JSON.parse(xhr.responseText);
                        console.log(organigramme);
                        callback(organigramme); 
                    }
                }
            };
            xhr.open("GET", "../inc/tree.php?idPoste=" + idPosteMere, true);
            xhr.send(null);
        }
        function majTree() {
            var xhr = getXhr();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        var organigramme = JSON.parse(xhr.responseText);
                        var tree = document.getElementById("tree");
                        tree.innerHTML = ""; 
                        genererListe(organigramme, tree);
                    }
                }
            };
            xhr.open("GET", "../inc/tree.php?idPoste=" + 0, true);
            xhr.send(null);
        }
        function genererListe(elements, elementParent) {
            var ul = document.createElement("ul");
            for (var i = 0; i < elements.length; i++) {
                var element = elements[i];
                var li = document.createElement("li");
                var a = document.createElement("a");
                a.href = "modifier.php?idPoste=" + element.idPoste;
                a.textContent = element.nom;
                getLi(element.idPoste, genererSousListe);
                function genererSousListe(elementEnfant) {
                    if (elementEnfant) {
                        genererListe(elementEnfant, li);
                    }
                }
                li.appendChild(a);
                ul.appendChild(li);
            }
            elementParent.appendChild(ul);
        }
    </script>
    <div class="container">
        <header>
            <center><h1>Organigramme de Gestion</h1></center>
        </header>
        <main>
            <div class="main">
                <div class="tree" id="tree">
                </div>
            </div>
        </main>
        <footer>

        </footer>
    </div>
</body>
</html>