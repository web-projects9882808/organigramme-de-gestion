<?php
require ('connexion.php');
function getOrganigramme($idPoste){
    $requete="SELECT * FROM PosteOrganigramme_view WHERE IdPosteMere=".$idPoste;
    $sql=mysqli_query(dbconnect(),$requete);
    return $sql;
}
header( "Content-Type: application/json");
$idPoste=$_GET["idPoste"];
$organigramme=getOrganigramme($idPoste);
$i=0;
$tab=array();
    while ($s=mysqli_fetch_assoc($organigramme)) {
        $tab[$i]=array(
            "idPoste"=>$s["IdPoste"], 
            "nom"=>$s["Nom"],
            "niveau"=>$s["Niveau"],
            "idPosteMere"=>$s["IdPosteMere"]
        );
        $i++;   
    }
echo json_encode($tab);
mysqli_free_result($organigramme);
?>